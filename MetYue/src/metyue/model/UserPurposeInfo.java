package metyue.model;
// Generated 2014-7-26 9:21:38 by Hibernate Tools 3.6.0


import java.util.Date;

/**
 * UserPurposeInfo generated by hbm2java
 */
public class UserPurposeInfo  implements java.io.Serializable {

	private static final long serialVersionUID = -304071041004962452L;
	private long userPtr;
     private UserInfo userInfo;
     private String plan;
     private String express;
     private String manner;
     private String purposeDesc;
     private Date lastUpdate;
     private Long lastUserPtr;

    public UserPurposeInfo() {
    }
	
    public UserPurposeInfo(long userPtr, String plan, String express,
			String manner, String purposeDesc, Date lastUpdate, Long lastUserPtr) {
		super();
		this.userPtr = userPtr;
		this.plan = plan;
		this.express = express;
		this.manner = manner;
		this.purposeDesc = purposeDesc;
		this.lastUpdate = lastUpdate;
		this.lastUserPtr = lastUserPtr;
	}

	public UserPurposeInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    public UserPurposeInfo(UserInfo userInfo, String plan, String express, String manner, String purposeDesc, Date lastUpdate, Long lastUserPtr) {
       this.userInfo = userInfo;
       this.plan = plan;
       this.express = express;
       this.manner = manner;
       this.purposeDesc = purposeDesc;
       this.lastUpdate = lastUpdate;
       this.lastUserPtr = lastUserPtr;
    }
   
    public long getUserPtr() {
        return this.userPtr;
    }
    
    public void setUserPtr(long userPtr) {
        this.userPtr = userPtr;
    }
    public UserInfo getUserInfo() {
        return this.userInfo;
    }
    
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    public String getPlan() {
        return this.plan;
    }
    
    public void setPlan(String plan) {
        this.plan = plan;
    }
    public String getExpress() {
        return this.express;
    }
    
    public void setExpress(String express) {
        this.express = express;
    }
    public String getManner() {
        return this.manner;
    }
    
    public void setManner(String manner) {
        this.manner = manner;
    }
    public String getPurposeDesc() {
        return this.purposeDesc;
    }
    
    public void setPurposeDesc(String purposeDesc) {
        this.purposeDesc = purposeDesc;
    }
    public Date getLastUpdate() {
        return this.lastUpdate;
    }
    
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public Long getLastUserPtr() {
        return this.lastUserPtr;
    }
    
    public void setLastUserPtr(Long lastUserPtr) {
        this.lastUserPtr = lastUserPtr;
    }




}


