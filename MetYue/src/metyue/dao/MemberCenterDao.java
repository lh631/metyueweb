package metyue.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import metyue.model.MemberCenterVO;
import metyue.model.UserFans;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MemberCenterDao {

	@Autowired
    private HibernateTemplate hibernateTemplate;
	
	////////////////////////////////////////////////////////////////////////////////	
	
	//增加或删除粉丝：弹性执行
	public void trunOnFans(long userPtr, long fansPtr){		
		
		UserFans itemExist = getFansInfo( userPtr,  fansPtr);
		
		if( (itemExist != null) && (itemExist.getPkey() > 0L) ){
			
			hibernateTemplate.delete(itemExist);
			
		}else{			
			
			UserFans item = new UserFans();
			
			item.setUserPtr(userPtr);
			item.setFansPtr(fansPtr);
			item.setLastUserPtr(userPtr);
			item.setLastUpdate(new Date());
			
			hibernateTemplate.saveOrUpdate(item);
		}
			
	}
	
	//是否存在 Fans 关系
	@SuppressWarnings("unchecked")
	public UserFans getFansInfo(long userPtr, long fansPtr){
			
		UserFans item = new UserFans();
		
		try {
			
		    String queryString = " FROM UserFans WHERE userPtr = ? and fansPtr = ?";
		    List<UserFans> lst = hibernateTemplate.find(queryString, userPtr, fansPtr);
		    
		    if(lst.size() > 0){	
		    	
		    	item = lst.get(0);
		    	
		    }
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}

	
	//细分关键词查询
	public String makeSql(String columnsName, String splits){
		
		String result = "";
		
		if(splits.length() > 0){
			
			String[] values = splits.split(" ");
			int size = values.length;
			
			for(int n=0; n < size; n++){
			
				result += columnsName + " like '%" + values[n] + "%' " + ( (n+1) == size? "" : " or ");
			}
			
			result = "(" + result + ")";
			
		}
		
		return result;
	}
	
	public MemberCenterVO makeClassMemberCenterVO(Object[] obj){
		
		MemberCenterVO item = new MemberCenterVO();
		
		try{
			
			item.setUserPtr(Long.valueOf(obj[0].toString()));
			item.setUserName(String.valueOf(obj[1]));
			item.setSexDesc(String.valueOf(obj[2]));
			item.setAge(Long.valueOf(obj[3].toString()));
			item.setCurrentAddress(String.valueOf(obj[4]));
			item.setHeight(Long.valueOf(obj[5].toString()));
			item.setWeight(Long.valueOf(obj[6].toString()));
			item.setMarryDesc(String.valueOf(obj[7]));
			item.setJobDesc(String.valueOf(obj[8]));
			item.setEduDesc(String.valueOf(obj[9]));
			item.setPlanDesc(String.valueOf(obj[10]));   
			item.setExpressDesc(String.valueOf(obj[11]));
			item.setMannerDesc(String.valueOf(obj[12]));
			item.setPurposeDesc(String.valueOf(obj[13]));
			item.setTencentNum(String.valueOf(obj[14]));
			item.setClassTencentNum(String.valueOf(obj[15]));
			item.setTencentWeixien(String.valueOf(obj[16]));
			item.setClassWeixien(String.valueOf(obj[17]));        
			item.setSinaWeibo(String.valueOf(obj[18]));
			item.setClassSinaWeibo(String.valueOf(obj[19]));
			item.setRenrenUrl(String.valueOf(obj[20]));
			item.setClassRenren(String.valueOf(obj[21]));
			item.setTelphone(String.valueOf(obj[22]));
			item.setClassTelphone(String.valueOf(obj[23]));
			item.setOthContactBy(String.valueOf(obj[24]));
			item.setClassOthContactBy(String.valueOf(obj[25]));
			item.setCoverImageUrl(String.valueOf(obj[26]));
			item.setOptFans(String.valueOf(obj[27]));
			item.setOptFocuser(String.valueOf(obj[28]));
			item.setOnlineStatusDesc(String.valueOf(obj[29]));
			item.setIsHaveContact(String.valueOf(obj[30]));
			
		} catch (Exception e) {
			
			item = null;
			
			e.printStackTrace();
	   	}
		 
		return item;
	}
	
	//会员中心的查询: list
	public List<MemberCenterVO> getUserPhotoInfoVOList(Map<String, String> para){
		
		List<MemberCenterVO> list = new ArrayList<MemberCenterVO>();
		
		try {
			
			String curUserPtrStr = (para.get("USER_CUR_PTR") != null) ? para.get("USER_CUR_PTR").toString() : "-1";
			
			final StringBuilder queryStr = new StringBuilder();
			queryStr.append(" SELECT USER_PTR,USER_NAME,");
			queryStr.append(" SEX_DESC,AGE,CURRENT_ADDRESS,HEIGHT,WEIGHT,MARRY_DESC,JOB_DESC,EDU_DESC,PLAN_DESC,EXPRESS_DESC,MANNER_DESC,PURPOSE_DESC,");
			queryStr.append(" TENCENT_NUM,CLASS_TENCENT_NUM,TENCENT_WEIXIEN,CLASS_WEIXIEN,");  
			queryStr.append(" SINA_WEIBO,CLASS_SINA_WEIBO,RENREN_URL,CLASS_RENREN,TELPHONE,CLASS_TELPHONE,OTH_CONTACT_BY,CLASS_OTH_CONTACT_BY,");
			queryStr.append(" COVER_IMAGE_URL,OPT_FANS,OPT_FANS_DESC,OPT_FOCUSER,OPT_FOCUSER_DESC,ONLINE_STATUS_DESC,IS_HAVE_CONTACT FROM (");
			queryStr.append(" SELECT DISTINCT ");
			queryStr.append(" USER.PKEY AS USER_PTR, ");
			queryStr.append(" USER.USER_NAME, ");
			queryStr.append(" (CASE WHEN USER.SEX='1' THEN '男生' WHEN USER.SEX='2' THEN '女生' ELSE '保密' END) AS SEX_DESC, ");
			queryStr.append(" BASE.AGE, ");
			queryStr.append(" IFNULL(BASE.CURRENT_ADDRESS,'') AS CURRENT_ADDRESS, ");
			queryStr.append(" BASE.HEIGHT, ");
			queryStr.append(" BASE.WEIGHT, ");
			queryStr.append(" (CASE WHEN BASE.MARRY='1' THEN '未婚' ");
			queryStr.append("      WHEN BASE.MARRY='2' THEN '已婚' ");
			queryStr.append("     WHEN BASE.MARRY='3' THEN '丧偶' ");
			queryStr.append("      ELSE '“若得山花插满头，莫问奴归处”' END) AS MARRY_DESC, ");
			queryStr.append(" (CASE WHEN BASE.JOB='1' THEN '办公行政' ");
			queryStr.append("     WHEN BASE.JOB='2' THEN '销售业务' ");
			queryStr.append("     WHEN BASE.JOB='3' THEN '产线工人' ");
			queryStr.append("     WHEN BASE.JOB='4' THEN '技术人员' ");
			queryStr.append("     WHEN BASE.JOB='5' THEN '在校学生' ");
			queryStr.append("     WHEN BASE.JOB='6' THEN '其它' ");
			queryStr.append("     ELSE '“革命不分先后，来到都是学生”' END) AS JOB_DESC, ");
			queryStr.append(" (CASE WHEN BASE.EDUCATION='1' THEN '初中' ");
			queryStr.append("     WHEN BASE.EDUCATION='2' THEN '高中' ");
			queryStr.append("     WHEN BASE.EDUCATION='3' THEN '中专' ");
			queryStr.append("     WHEN BASE.EDUCATION='4' THEN '专科' ");
			queryStr.append("     WHEN BASE.EDUCATION='5' THEN '本科' ");
			queryStr.append("     WHEN BASE.EDUCATION='6' THEN '硕士' ");
			queryStr.append("     WHEN BASE.EDUCATION='7' THEN '博士' ");
			queryStr.append("     WHEN BASE.EDUCATION='8' THEN '其它' ");
			queryStr.append("     ELSE '“小学毕业，大学文化，博士人生”' END) AS EDU_DESC, ");
			            
			queryStr.append(" (CASE WHEN PURPOSE.PLAN='1' THEN '征友，寻找拥有共同兴趣的朋友' ");
			queryStr.append("     WHEN PURPOSE.PLAN='2' THEN '征婚，寻觅人生的另一半' ");
			queryStr.append("     WHEN PURPOSE.PLAN='3' THEN '合作，寻求生活或事业的新激情' ");
			queryStr.append("     WHEN PURPOSE.PLAN='4' THEN '帮助，寻求经济或道义上的支持' ");
			queryStr.append("     ELSE '“五洲风雷云水怒，四海之内皆兄弟”' END) AS PLAN_DESC, ");  
			queryStr.append("  (CASE WHEN PURPOSE.EXPRESS='1' THEN '没有发生过，想尝试一下' ");
			queryStr.append("     WHEN PURPOSE.EXPRESS='2' THEN '曾经发生过，乐意再尝试' ");
			queryStr.append("     WHEN PURPOSE.EXPRESS='3' THEN '已尝试多次，喜欢这感觉' ");
			queryStr.append("     WHEN PURPOSE.EXPRESS='4' THEN '特别迷恋它，很想经常玩' ");
			queryStr.append("     ELSE '“此地无银三百两，隔壁王二不曾偷”，谢谢' END) AS EXPRESS_DESC, ");
			queryStr.append(" (CASE WHEN PURPOSE.MANNER='1' THEN '先见一面相互认识' ");
			queryStr.append("     WHEN PURPOSE.MANNER='2' THEN '一切随缘而为吧' ");
			queryStr.append("     WHEN PURPOSE.MANNER='3' THEN '过了今晚，我们没有明天' ");
			queryStr.append("     WHEN PURPOSE.MANNER='4' THEN '彼此满意可做长期打算' ");
			queryStr.append("     WHEN PURPOSE.MANNER='5' THEN '保密，不想回答' ");
			queryStr.append("     ELSE '“小楼一夜听春雨，明朝深巷卖杏花”' END) AS MANNER_DESC, ");
			queryStr.append(" IFNULL(PURPOSE.PURPOSE_DESC,'〈谁轻轻叫唤我，唤醒心中爱火，幸运只因有着你，不再流浪与磋砣。。。无可奉告，谢谢〉') AS PURPOSE_DESC, ");
			        
			queryStr.append(" IFNULL(CONTACT.TENCENT_NUM,'') AS TENCENT_NUM, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.TENCENT_NUM,''))=0 THEN 'displayNoneQQ' ELSE 'displayQQ' END) AS CLASS_TENCENT_NUM, ");
			queryStr.append(" IFNULL(CONTACT.TENCENT_WEIXIEN, '') AS TENCENT_WEIXIEN, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.TENCENT_WEIXIEN,''))=0 THEN 'displayNoneWX' ELSE 'displayWX' END) AS CLASS_WEIXIEN,  ");       
			queryStr.append(" IFNULL(CONTACT.SINA_WEIBO, '') AS SINA_WEIBO, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.SINA_WEIBO,''))=0 THEN 'displayNoneWB' ELSE 'displayWB' END) AS CLASS_SINA_WEIBO, ");
			queryStr.append(" IFNULL(CONTACT.RENREN_URL,'') AS RENREN_URL, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.RENREN_URL,''))=0 THEN 'displayNoneRR' ELSE 'displayRR' END) AS CLASS_RENREN, ");
			queryStr.append(" IFNULL(CONTACT.TEL,'') AS TELPHONE, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.TEL,''))=0 THEN 'displayNoneTel' ELSE 'displayTel' END) AS CLASS_TELPHONE, ");
			queryStr.append(" IFNULL(CONTACT.OTH_CONTACT_BY,'') AS OTH_CONTACT_BY, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.OTH_CONTACT_BY,''))=0 THEN 'displayNoneOTH' ELSE 'displayOTH' END) AS CLASS_OTH_CONTACT_BY, ");
			        
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(PHOTO_LIST.FILE_NAME,''))>0 THEN  ");
			queryStr.append("     CONCAT('resources',PHOTO_LIST.URL_PATH,PHOTO.USER_PTR,'/r_',PHOTO_LIST.FILE_NAME) ");
			queryStr.append("     ELSE 'resources/style/images/cover.gif' ");
			queryStr.append("     END) AS COVER_IMAGE_URL, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN 'O' WHEN (IFNULL(FANS.FANS_PTR,'0'))>'0' THEN 'T' ELSE 'F' END) AS OPT_FANS, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN '' WHEN (IFNULL(FANS.FANS_PTR,'0'))>'0' THEN '取消关注' ELSE '关注对方' END) AS OPT_FANS_DESC, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN 'O' WHEN (IFNULL(FOCUSER.FANS_PTR,'0'))>'0' THEN 'T' ELSE 'F' END) AS OPT_FOCUSER, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN '' WHEN (IFNULL(FOCUSER.FANS_PTR,'0'))>'0' THEN '我的粉丝' ELSE '路人甲' END) AS OPT_FOCUSER_DESC, ");
			
			queryStr.append(" (CASE WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) > '172800' THEN '二天前在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) > '86400' THEN '一天前在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) > '3600' THEN '一天内在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) BETWEEN '1801' AND '3600' THEN '一小时内在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) BETWEEN '900' AND '1800' THEN '30分钟内在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) BETWEEN '0' AND '900' THEN '在线'   ");
			queryStr.append("     ELSE '三天前在线' END) AS ONLINE_STATUS_DESC, ");
			queryStr.append(" ((LENGTH(IFNULL(CONTACT.TENCENT_NUM,''))>0) OR (LENGTH(IFNULL(CONTACT.TENCENT_WEIXIEN,''))>0) ");
			queryStr.append(" OR (LENGTH(IFNULL(CONTACT.SINA_WEIBO,''))>0) OR (LENGTH(IFNULL(CONTACT.RENREN_URL,''))>0) ");
			queryStr.append(" OR (LENGTH(IFNULL(CONTACT.TEL,''))>0)) AS IS_HAVE_CONTACT ");
			queryStr.append(" FROM USER_INFO AS USER ");
			queryStr.append(" LEFT JOIN USER_BASE_INFO AS BASE ON USER.PKEY=BASE.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_PURPOSE_INFO AS PURPOSE ON USER.PKEY=PURPOSE.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_CONTACT_INFO AS CONTACT ON USER.PKEY=CONTACT.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_PHOTO_INFO AS PHOTO ON USER.PKEY=PHOTO.USER_PTR AND PHOTO.NAME='DEFAULT' ");
			queryStr.append(" LEFT JOIN USER_PHOTO_LIST_INFO AS PHOTO_LIST ON PHOTO_LIST.GROUP_BY_PTR=PHOTO.PKEY AND PHOTO_LIST.IS_COVER='1' ");
			queryStr.append(" LEFT JOIN USER_FANS AS FANS ON FANS.USER_PTR=USER.PKEY AND FANS.FANS_PTR='" + curUserPtrStr+"'");
			queryStr.append(" LEFT JOIN USER_FANS AS FOCUSER ON FOCUSER.FANS_PTR=USER.PKEY AND FOCUSER.USER_PTR='" + curUserPtrStr+"'");
			queryStr.append(" LEFT JOIN (SELECT USER_PTR, MAX(LAST_UPDATE) AS LAST_UPDATE FROM USER_OPT_LOGO GROUP BY USER_PTR) AS ONLINE ON ONLINE.USER_PTR=USER.PKEY ");
			queryStr.append(" WHERE (1=1) ");
			
			//避免查询到自己的信息
			queryStr.append(" and USER.PKEY <> '" + curUserPtrStr+"'" );
			
			String tmpStr = "";
		    if(para.get("USER_PTR") != null){
		    	
		    	tmpStr = para.get("USER_PTR").toString();
		    	queryStr.append( !"".equals(tmpStr) ? " and (USER.PKEY='" + tmpStr + "') ": "" );
		    }
		    
		    //关键词		    
		    String keyWord = para.get("KEY_WORDS");
		    if((keyWord != null) && (keyWord.length() > 0)){
		    	
		    	StringBuffer sqlTemp = new StringBuffer();
		    	sqlTemp.append(" and ( ");
		    	String [] keys  = keyWord.split(" ");
		    	for(int i = 0;i < keys.length;i++ ){		    		
		    		sqlTemp.append(" (USER.USER_NAME like '%" + keys[i] + "%' ) OR ");
		    		sqlTemp.append(" (BASE.CURRENT_ADDRESS like '%" + keys[i] + "%' ) OR ");
		    		sqlTemp.append(" (PURPOSE.PURPOSE_DESC like '%" + keys[i] + "%' ) OR ");
		    		sqlTemp.append(" (CONTACT.TENCENT_NUM like '%" + keys[i] + "%' ) OR ");
		    		sqlTemp.append(" (CONTACT.TEL like '%" + keys[i] + "%' ) OR ");
		    		sqlTemp.append(" (CONTACT.SINA_WEIBO like '%" + keys[i] + "%' ) ");
		    	}
		    	sqlTemp.append(" ) ");		    	
		    	queryStr.append( sqlTemp.toString() );
		    }	
		    
		    
		    if(para.get("QUERY_SEX") != null){
		    	
		    	String sex = para.get("QUERY_SEX").toString();		    	
		    	switch(sex){
		    		case "1":
		    			queryStr.append( " and (USER.SEX='1')" );
		    			break;
		    		case "2":
		    			queryStr.append( " and (USER.SEX='2')" );
		    			break;		    		
		    	}
		    }
		    
		    if(para.get("QUERY_PLAN") != null){
		    	
		    	String plan = para.get("QUERY_PLAN").toString();	
		    	queryStr.append( ("1".equals(plan) || "2".equals(plan)) ? " and (PURPOSE.PLAN='" + plan + "')" : "" );
		    }
		    
		    queryStr.append( " and (BASE.AGE between " + (!"".equals(para.get("QUERY_AGE1")) ? para.get("QUERY_AGE1").toString() : "14") 
		    		+ " and " + (!"".equals(para.get("QUERY_AGE2")) ? para.get("QUERY_AGE2").toString() : "60") 
		    		+ ")" );
		    
		    queryStr.append(" )AAA ");
		    
		    queryStr.append(" WHERE (1=1) ");	//标识性字段名称太长，所以写在外面   
		    
		    //我的粉丝
		    if(para.get("OPT_FANS") != null){		    	

		    	tmpStr = para.get("OPT_FANS").toString();
		    	queryStr.append( "T".equals(tmpStr) ? " and (OPT_FANS='T') " : "" );
		    } 
		    
		    //我的关注
		    if(para.get("OPT_FOCUSER") != null){
		    	
		    	tmpStr = para.get("OPT_FOCUSER").toString();
		    	queryStr.append( "T".equals(tmpStr) ? " and (OPT_FOCUSER='T') " : "" );
		    }
		    
		    //是否在线
		    if(para.get("OPT_ONLINE") != null){
		    	
		    	tmpStr = para.get("OPT_ONLINE").toString();
		    	queryStr.append( "T".equals(tmpStr) ? " and (ONLINE_STATUS_DESC='在线') " : "" );
		    }
		    
		    //是否有联系方式
		    if(para.get("OPT_HAVE_CONTACT") != null){
		    	
		    	tmpStr = para.get("OPT_HAVE_CONTACT").toString();
		    	queryStr.append( "T".equals(tmpStr) ? " and (IS_HAVE_CONTACT=1) " : "" );
		    }
		    
		    list = (List<MemberCenterVO>) this.hibernateTemplate.execute(new HibernateCallback<List<MemberCenterVO>>() {
		    	
		    	@SuppressWarnings("unchecked")
				public List<MemberCenterVO> doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString()).addEntity(MemberCenterVO.class);
		    		
		    		return query.list();
		    	}
		    });
		    
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
	//显示所指定的会员资料: list
	public List<MemberCenterVO> getUserPhotoInfoVOByUserPtr(Map<String, String> para){
		
		List<MemberCenterVO> list = new ArrayList<MemberCenterVO>();
		
		try {
			
			String curUserPtrStr = (para.get("USER_CUR_PTR") != null) ? para.get("USER_CUR_PTR").toString() : "-1";
			
			final StringBuilder queryStr = new StringBuilder();
			queryStr.append(" SELECT ");
			queryStr.append(" USER.PKEY AS USER_PTR, ");
			queryStr.append(" USER.USER_NAME, ");
			queryStr.append(" (CASE WHEN USER.SEX='1' THEN '男生' WHEN USER.SEX='2' THEN '女生' ELSE '保密' END) AS SEX_DESC, ");
			queryStr.append(" BASE.AGE, ");
			queryStr.append(" IFNULL(BASE.CURRENT_ADDRESS,'') AS CURRENT_ADDRESS, ");
			queryStr.append(" BASE.HEIGHT, ");
			queryStr.append(" BASE.WEIGHT, ");
			queryStr.append(" (CASE WHEN BASE.MARRY='1' THEN '未婚' ");
			queryStr.append("      WHEN BASE.MARRY='2' THEN '已婚' ");
			queryStr.append("     WHEN BASE.MARRY='3' THEN '丧偶' ");
			queryStr.append("      ELSE '“若得山花插满头，莫问奴归处”' END) AS MARRY_DESC, ");
			queryStr.append(" (CASE WHEN BASE.JOB='1' THEN '办公行政' ");
			queryStr.append("     WHEN BASE.JOB='2' THEN '销售业务' ");
			queryStr.append("     WHEN BASE.JOB='3' THEN '产线工人' ");
			queryStr.append("     WHEN BASE.JOB='4' THEN '技术人员' ");
			queryStr.append("     WHEN BASE.JOB='5' THEN '在校学生' ");
			queryStr.append("     WHEN BASE.JOB='6' THEN '其它' ");
			queryStr.append("     ELSE '“革命不分先后，来到都是学生”' END) AS JOB_DESC, ");
			queryStr.append(" (CASE WHEN BASE.EDUCATION='1' THEN '初中' ");
			queryStr.append("     WHEN BASE.EDUCATION='2' THEN '高中' ");
			queryStr.append("     WHEN BASE.EDUCATION='3' THEN '中专' ");
			queryStr.append("     WHEN BASE.EDUCATION='4' THEN '专科' ");
			queryStr.append("     WHEN BASE.EDUCATION='5' THEN '本科' ");
			queryStr.append("     WHEN BASE.EDUCATION='6' THEN '硕士' ");
			queryStr.append("     WHEN BASE.EDUCATION='7' THEN '博士' ");
			queryStr.append("     WHEN BASE.EDUCATION='8' THEN '其它' ");
			queryStr.append("     ELSE '“小学毕业，大学文化，博士人生”' END) AS EDU_DESC, ");
			            
			queryStr.append(" (CASE WHEN PURPOSE.PLAN='1' THEN '征友，寻找拥有共同兴趣的朋友' ");
			queryStr.append("     WHEN PURPOSE.PLAN='2' THEN '征婚，寻觅人生的另一半' ");
			queryStr.append("     WHEN PURPOSE.PLAN='3' THEN '合作，寻求生活或事业的新激情' ");
			queryStr.append("     WHEN PURPOSE.PLAN='4' THEN '帮助，寻求经济或道义上的支持' ");
			queryStr.append("     ELSE '“五洲风雷云水怒，四海之内皆兄弟”' END) AS PLAN_DESC, ");  
			queryStr.append("  (CASE WHEN PURPOSE.EXPRESS='1' THEN '没有发生过，想尝试一下' ");
			queryStr.append("     WHEN PURPOSE.EXPRESS='2' THEN '曾经发生过，乐意再尝试' ");
			queryStr.append("     WHEN PURPOSE.EXPRESS='3' THEN '已尝试多次，喜欢这感觉' ");
			queryStr.append("     WHEN PURPOSE.EXPRESS='4' THEN '特别迷恋它，很想经常玩' ");
			queryStr.append("     ELSE '“此地无银三百两，隔壁王二不曾偷”，谢谢' END) AS EXPRESS_DESC, ");
			queryStr.append(" (CASE WHEN PURPOSE.MANNER='1' THEN '先见一面相互认识' ");
			queryStr.append("     WHEN PURPOSE.MANNER='2' THEN '一切随缘而为吧' ");
			queryStr.append("     WHEN PURPOSE.MANNER='3' THEN '过了今晚，我们没有明天' ");
			queryStr.append("     WHEN PURPOSE.MANNER='4' THEN '彼此满意可做长期打算' ");
			queryStr.append("     WHEN PURPOSE.MANNER='5' THEN '保密，不想回答' ");
			queryStr.append("     ELSE '“小楼一夜听春雨，明朝深巷卖杏花”' END) AS MANNER_DESC, ");
			queryStr.append(" IFNULL(PURPOSE.PURPOSE_DESC,'〈谁轻轻叫唤我，唤醒心中爱火，幸运只因有着你，不再流浪与磋砣。。。无可奉告，谢谢〉') AS PURPOSE_DESC, ");
			        
			queryStr.append(" IFNULL(CONTACT.TENCENT_NUM,'') AS TENCENT_NUM, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.TENCENT_NUM,''))=0 THEN 'displayNoneQQ' ELSE 'displayQQ' END) AS CLASS_TENCENT_NUM, ");
			queryStr.append(" IFNULL(CONTACT.TENCENT_WEIXIEN, '') AS TENCENT_WEIXIEN, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.TENCENT_WEIXIEN,''))=0 THEN 'displayNoneWX' ELSE 'displayWX' END) AS CLASS_WEIXIEN,  ");       
			queryStr.append(" IFNULL(CONTACT.SINA_WEIBO, '') AS SINA_WEIBO, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.SINA_WEIBO,''))=0 THEN 'displayNoneWB' ELSE 'displayWB' END) AS CLASS_SINA_WEIBO, ");
			queryStr.append(" IFNULL(CONTACT.RENREN_URL,'') AS RENREN_URL, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.RENREN_URL,''))=0 THEN 'displayNoneRR' ELSE 'displayRR' END) AS CLASS_RENREN, ");
			queryStr.append(" IFNULL(CONTACT.TEL,'') AS TELPHONE, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.TEL,''))=0 THEN 'displayNoneTel' ELSE 'displayTel' END) AS CLASS_TELPHONE, ");
			queryStr.append(" IFNULL(CONTACT.OTH_CONTACT_BY,'') AS OTH_CONTACT_BY, ");
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(CONTACT.OTH_CONTACT_BY,''))=0 THEN 'displayNoneOTH' ELSE 'displayOTH' END) AS CLASS_OTH_CONTACT_BY, ");
			        
			queryStr.append(" (CASE WHEN LENGTH(IFNULL(PHOTO_LIST.FILE_NAME,''))>0 THEN  ");
			queryStr.append("     CONCAT('resources',PHOTO_LIST.URL_PATH,PHOTO.USER_PTR,'/r_',PHOTO_LIST.FILE_NAME) ");
			queryStr.append("     ELSE 'resources/style/images/cover.gif' ");
			queryStr.append("     END) AS COVER_IMAGE_URL, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN 'O' WHEN (IFNULL(FANS.FANS_PTR,'0'))>'0' THEN 'T' ELSE 'F' END) AS OPT_FANS, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN '' WHEN (IFNULL(FANS.FANS_PTR,'0'))>'0' THEN '取消关注' ELSE '关注对方' END) AS OPT_FANS_DESC, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN 'O' WHEN (IFNULL(FOCUSER.FANS_PTR,'0'))>'0' THEN 'T' ELSE 'F' END) AS OPT_FOCUSER, ");
			queryStr.append(" (CASE WHEN '" + curUserPtrStr+"'='-1' THEN '' WHEN (IFNULL(FOCUSER.FANS_PTR,'0'))>'0' THEN '我的粉丝' ELSE '路人甲' END) AS OPT_FOCUSER_DESC, ");
			
			queryStr.append(" (CASE WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) > '172800' THEN '二天前在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) > '86400' THEN '一天前在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) > '3600' THEN '一天内在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) BETWEEN '1801' AND '3600' THEN '一小时内在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) BETWEEN '900' AND '1800' THEN '30分钟内在线' ");
			queryStr.append("     WHEN (time_to_sec(timediff(now() , ONLINE.LAST_UPDATE))) BETWEEN '0' AND '900' THEN '在线'   ");
			queryStr.append("     ELSE '三天前在线' END) AS ONLINE_STATUS_DESC, ");
			queryStr.append(" ((LENGTH(IFNULL(CONTACT.TENCENT_NUM,''))>0) OR (LENGTH(IFNULL(CONTACT.TENCENT_WEIXIEN,''))>0) ");
			queryStr.append(" OR (LENGTH(IFNULL(CONTACT.SINA_WEIBO,''))>0) OR (LENGTH(IFNULL(CONTACT.RENREN_URL,''))>0) ");
			queryStr.append(" OR (LENGTH(IFNULL(CONTACT.TEL,''))>0)) AS IS_HAVE_CONTACT ");
			queryStr.append(" FROM USER_INFO AS USER ");
			queryStr.append(" LEFT JOIN USER_BASE_INFO AS BASE ON USER.PKEY=BASE.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_PURPOSE_INFO AS PURPOSE ON USER.PKEY=PURPOSE.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_CONTACT_INFO AS CONTACT ON USER.PKEY=CONTACT.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_PHOTO_INFO AS PHOTO ON USER.PKEY=PHOTO.USER_PTR AND PHOTO.NAME='DEFAULT' ");
			queryStr.append(" LEFT JOIN USER_PHOTO_LIST_INFO AS PHOTO_LIST ON PHOTO_LIST.GROUP_BY_PTR=PHOTO.PKEY AND PHOTO_LIST.IS_COVER='1' ");
			queryStr.append(" LEFT JOIN USER_FANS AS FANS ON FANS.FANS_PTR=USER.PKEY AND FANS.USER_PTR='" + curUserPtrStr+"'");
			queryStr.append(" LEFT JOIN USER_FANS AS FOCUSER ON FOCUSER.USER_PTR=USER.PKEY AND FOCUSER.FANS_PTR='" + curUserPtrStr+"'");
			queryStr.append(" LEFT JOIN (SELECT USER_PTR, MAX(LAST_UPDATE) AS LAST_UPDATE FROM USER_OPT_LOGO GROUP BY USER_PTR) AS ONLINE ON ONLINE.USER_PTR=USER.PKEY ");
			queryStr.append(" WHERE (1=1) ");
			
			String tmpStr = "";
		    if(para.get("USER_PTR") != null){
		    	
		    	tmpStr = para.get("USER_PTR").toString();
		    	queryStr.append( !"".equals(tmpStr) ? " and (USER.PKEY='" + tmpStr + "') ": "" );
		    }
		    
		    list = (List<MemberCenterVO>) this.hibernateTemplate.execute(new HibernateCallback<List<MemberCenterVO>>() {
		    	
		    	@SuppressWarnings("unchecked")
				public List<MemberCenterVO> doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString()).addEntity(MemberCenterVO.class);
		    		
		    		return query.list();
		    	}
		    });
		    		    
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}

}
