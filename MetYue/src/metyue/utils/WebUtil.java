package metyue.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import metyue.model.ActionResult;
import metyue.model.UserSessionInfo;

public class WebUtil {
	
	//AAA
	public static String USER_PHOTO_PYSI_FILE_PATH = "/usr/local/apache/apache-tomcat-7.0.54/webapps/metyue/resources/UserInfo/images/"; //"D:/UserInfo/images/";//
	public static String USER_PHOTO_URL = "/UserInfo/images/";
	public static String USER_PHOTO_INIT_STATUS = "1";
	public static String USER_PHOTO_CHECK_PREFOX = "F_"; //被禁止的图片前缀
	
	//BBB
	public static Integer USER_BASE_INFO_MAN_AGE = 25;
	public static Integer USER_BASE_INFO_GIRL_AGE = 22;	
	public static Integer USER_BASE_INFO_MAN_HEIGHT = 172;
	public static Integer USER_BASE_INFO_GIRL_HEIGHT = 162;
	public static Integer USER_BASE_INFO_MAN_WEIGHT = 65;
	public static Integer USER_BASE_INFO_GIRL_WEIGHT = 45;
	public static String USER_BASE_INFO_MARRY = "1";
	public static String USER_BASE_INFO_JOB = "1";
	public static String USER_BASE_INFO_EDUCATION = "4";
	
	public static String USER_PURPOSE_INFO_PLAN = "1";
	public static String USER_PURPOSE_INFO_EXPRESS = "1";
	public static String USER_PURPOSE_INFO_MANNER = "1";
	
	public static String USER_PHOTO_DEFAULT_NAME = "Default";
	
	//CCC
	public static String OPT_LOGO_INSERT = "INSERT";
	public static String OPT_LOGO_UPDATE = "UPDATE";
	public static String OPT_LOGO_DELETE = "DELETE";
	public static String OPT_LOGO_LOGIN = "LOGIN";
	public static String OPT_LOGO_LOGOUT = "LOGOUT";
	public static String OPT_LOGO_REG = "REGISTER";
	public static String OPT_LOGO_AUTO_OFF_LINE = "AUTO_OFFLINE";
	
	//DDD
	public static String SAVE_USER_BASE_INFO = "SAVE_USER_BASE_INFO";
	public static String SAVE_USER_PURPOSE_INFO = "SAVE_USER_PURPOSE_INFO";
	public static String SAVE_USER_CONTACT_INFO = "SAVE_USER_CONTACT_INFO";
	public static String SAVE_USER_PHOTO_INFO = "SAVE_USER_PHOTO_INFO";
	public static String SAVE_USER_PHOTO_LIST_INFO = "SAVE_USER_PHOTO_LIST_INFO";
	
	//EEE
	public static String USER_FORGET_PWD = "USER_FORGET_PWD";
	public static String USER_CHANGE_PWD = "USER_CHANGE_PWD";
	
	//FFF
	public static String USER_CHANGE_PHOTO_COVER = "USER_CHANGE_PHOTO_COVER";
	public static String USER_DEL_PHOTO_IMAGE = "USER_DEL_PHOTO_IMAGE";
	
	//HHH
	public static String MEMBER_TRUN_ON_FANS = "MEMBER_TRUN_ON_FANS";
	public static String MEMBER_TRUN_ON_FOCUSER = "MEMBER_TRUN_ON_FOCUSER";
	
	//RRR
	public static String MANAGE_CHANGE_USER_NAME = "MANAGE_CHANGE_USER_NAME";
	public static String MANAGE_CHANGE_USER_PURPOSE = "MANAGE_CHANGE_USER_PURPOSE";
	public static String MANAGE_CHANGE_USER_IMAGE_CHECK = "MANAGE_CHANGE_USER_IMAGE_CHECK";
	public static String MANAGE_CHANGE_USER_IMAGE_MESSAGE = "按互联网相关法律法则的要求，您的一张图片可能涉嫌内容不健康等因素，已被网站管理员删除，敬请留意。谢谢！";
	
	public static void responseResultToJson( HttpServletResponse response, ActionResult item ){
		
		response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        
		try {
			
			Gson gson = new Gson();		
			response.getWriter().write( gson.toJson(item) );
			response.getWriter().flush();
			
		} catch (IOException e) {			
			e.printStackTrace();	
			
		} 
		
	}
	
	public static void responseToJson( HttpServletResponse response, Object obj ){
		
		response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        
		try {
			
			Gson gson = new Gson();		
			response.getWriter().write( gson.toJson(obj) );
			response.getWriter().flush();
			
		} catch (IOException e) {			
			e.printStackTrace();	
			
		} 
		
	}
	
	public static void responseToJsonString( HttpServletResponse response, StringBuffer stringBuffer){
		
		response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        
		try {
			
			response.getWriter().write( stringBuffer.toString() );
			response.getWriter().flush();
			
		} catch (IOException e) {			
			e.printStackTrace();	
			
		} 
		
	}
	
	public static void saveSession( HttpServletRequest request, String sessionName, UserSessionInfo userInfoItem){
		
		HttpSession session = request.getSession(); 
		session.setAttribute(sessionName, userInfoItem);
		
	}
	
	public static void removeSession( HttpServletRequest request, String sessionName){
		
		HttpSession session = request.getSession();
		session.removeAttribute(sessionName); 
		
	}
	
	public static UserSessionInfo getUserSession( HttpServletRequest request, String sessionName){
		
		UserSessionInfo item = null;
		HttpSession session = request.getSession();
		item = (UserSessionInfo)session.getAttribute(sessionName);
		
		return item;		
	}
	
	public static String decode2(String s){			
		
		try {
			
			if(s != null){
				
				s = URLDecoder.decode( s ,"UTF-8");
			}			
			
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		}
		
		return s;
	}
	
	//返回一个 8 位的新密码
	public static String makeUserNewPwd(){
		
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String pwd = df.format(new Date());
		
		return (MD5Util.MD5(pwd)).substring(0, 8);
	}
}

































