package metyue.service;

import java.util.List;

import metyue.dao.ContentManageDao;
import metyue.model.FilterContent;
import metyue.model.FilterUserImage;
import metyue.model.UserPhotoListInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContentManageService {

	@Autowired
	private ContentManageDao contentManageDao;
	
	
	//敏感：修改名称
	public void filterUserName(long userPtr, String userName){

		contentManageDao.filterUserName(userPtr, userName);
	}
	
	//敏感：交友宣言
	public void filterPurpose(long userPtr, String purposeDesc){

		contentManageDao.filterPurpose(userPtr, purposeDesc);
	}
	

	public void checkUploadImage(long userPtr, long photoListPtr){

		contentManageDao.checkUploadImage(userPtr, photoListPtr);
	}	
	

	public UserPhotoListInfo getImageCheckStatus(long photoListPtr){
		
		return contentManageDao.getImageCheckStatus(photoListPtr);
	}
	
	//敏感词查询
	public List<FilterContent> getFilterLists(String keyWords){
		
		return contentManageDao.getFilterLists(keyWords);
	}
	
	
	//敏感词查询
	public List<FilterUserImage> getFilterImageLists(String userName, String imageFileName){
		
		return contentManageDao.getFilterImageLists(userName, imageFileName);	
	}
	
}
