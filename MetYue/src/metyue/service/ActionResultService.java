package metyue.service;

import metyue.model.ActionResult;

import org.springframework.stereotype.Service;

@Service
public class ActionResultService {

	public ActionResult getActionResultItem(String code, String context){
		
		String result = "";
		
		if("10002".equals(code)){
			
			result = "FAIL";
			context = "";
			
		}else if("10003".equals(code)){
			
			result = "SUCCESS";
			
		}else if("10004".equals(code)){
			
			result = "FAIL";
			context = "很遗憾，您的密码问题回答不一致，如果确实是想不起来了，可以邮件至: Service@MetYue.Com";
			
		}else if("10005".equals(code)){
			
			result = "SUCCESS";
			context = "好吧，您的新密码是：" + context;
			
		}else if("10006".equals(code)){
			
			result = "FAIL";
			context = "嗯嗯，江湖上并没有关于此邮箱的传说，请确认没有输入错误";
			
		}else if("10007".equals(code)){
			
			result = "SUCCESS";
			
		}else if("10008".equals(code)){
			
			result = "FAIL";
			context = "0";
			
		}
		
		return new ActionResult(code, result, context);
	}
	
	public ActionResult getActionResultItem(String code){
		
		String result = "";
		String context = "";
		
		if("10000".equals(code)){
			
			result = "EXCEPTION";
			context = "问题貌似很严重喔，发生了一个百年不遇的技术类故障，可以重试之前的操作";			
		}else if("10001".equals(code)){
			
			result = "ERROR";
			context = "所调用的方法缺少必要的参数，请详细阅读 API 手册";			
		}else if("20001".equals(code)){
			
			result = "SUCCESS";
			context = "亲，此邮箱可以注册帐号";
		}else if("20002".equals(code)){
			
			result = "FAIL";
			context = "亲，此邮箱已被注册使用，可以换另一个哈";
		}else if("20003".equals(code)){
			
			result = "SUCCESS";
			context = "亲，注册成功，恭喜您！我们的口号是：过了今晚我们才有明天";
		}else if("20004".equals(code)){
			
			result = "FAIL";
			context = "可能是隔壁大叔大妈的气场太强，严重影响并且直接导致您的注册失败，可以重试注册";
		}else if("20005".equals(code)){
			
			result = "SUCCESS";
			context = "登录成功！现在您可以为全球男女关系的和谐，再抺上几道亮彩的蓝天白云，谢谢";
		}else if("20006".equals(code)){
			
			result = "FAIL";
			context = "如果不是罗密欧猜错茱丽叶的房间电话号码，那么就是您输错了密码";
		}else if("20007".equals(code)){
			
			result = "ON_LINE";
			context = "很好很强大，您仍然与 MetYue.Com 保持连线，这是一个无比英明的选择，谢谢";
		}else if("20008".equals(code)){
			
			result = "OFF_LINE";
			context = "网站连接因超时而中断，如要继续娱乐大众请重新登录";
		}else if("20009".equals(code)){
			
			result = "SUCCESS";
			context = "退出成功！谢谢您的支持";
		}else if("20010".equals(code)){
			
			result = "AUTO_OFF_LINE";
			context = "谢谢您的支持";
		}else if("20011".equals(code)){
			
			result = "SUCCESS";
			context = "成功保存";
		}else if("20012".equals(code)){
			
			result = "AUTO_OFF_LINE";
			context = "保存失败";
		}else if("20013".equals(code)){
			
			result = "SUCCESS";
			context = "上传成功";
		}else if("20014".equals(code)){
			
			result = "FAIL";
			context = "上传失败";
		}else if("20015".equals(code)){
			
			result = "SUCCESS";
			context = "上传成功";
		}else if("20016".equals(code)){
			
			result = "FAIL";
			context = "您输入的旧密码与您原先的密码不一致";
		}else if("20017".equals(code)){
			
			result = "SUCCESS";
			context = "您实在太细心了，前后两次的新密码输入完全一致";
		}else if("20018".equals(code)){
			
			result = "FAIL";
			context = "哼哼，哥们姐们，您前后两次的新密码输入不一致";
		}else if("20019".equals(code)){
			
			result = "SUCCESS";
			context = "操作成功！岁月安好，平安没事";
		}else if("20020".equals(code)){
			
			result = "FAIL";
			context = "操作失败！当前风云际会，命运之神露出了一丝诡异的神色";
		}else if("20021".equals(code)){
			
			result = "SUCCESS";
			context = "操作成功！缘来则聚，缘去则散，如此而已";
		}else if("20022".equals(code)){
			
			result = "FAIL";
			context = "操作失败！亲，自己关注自己，这叫自恋喔";
		}else if("20023".equals(code)){
			
			result = "SUCCESS";
			context = "“十样蛮笺纹错绮，粲珠玑、渊掷惊风雨”，消息已发送";
		}else if("20024".equals(code)){
			
			result = "FAIL";
			context = "“南云雁少，锦书无个因依”，消息没有发送";
		}else if("20025".equals(code)){
			
			result = "SUCCESS";
			context = "磁盘删除文件成功";
		}else if("20026".equals(code)){
			
			result = "FAIL";
			context = "磁盘删除文件失败";
		}else if("30001".equals(code)){
			
			result = "SUCCESS";
			context = "确认身份是管理人员";
		}else if("30002".equals(code)){
			
			result = "FAIL";
			context = "确认身份非管理人员，拒绝任何操作";
		}else if("30003".equals(code)){
			
			result = "SUCCESS";
			context = "操作成功";
		}else if("30004".equals(code)){
			
			result = "FAIL";
			context = "操作失败";
		}
		
		return new ActionResult(code, result, context);
	}
}

























































