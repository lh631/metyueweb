CREATE DATABASE  IF NOT EXISTS `metyuedb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `metyuedb`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: metyuedb
-- ------------------------------------------------------
-- Server version	5.5.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `user_talk_not_read_count_view`
--

DROP TABLE IF EXISTS `user_talk_not_read_count_view`;
/*!50001 DROP VIEW IF EXISTS `user_talk_not_read_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_talk_not_read_count_view` (
  `SEND_USER_PTR` bigint(20),
  `RECEIVE_USER_PTR` bigint(20),
  `NOT_READ_COUNT` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cover_image_url_view`
--

DROP TABLE IF EXISTS `cover_image_url_view`;
/*!50001 DROP VIEW IF EXISTS `cover_image_url_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cover_image_url_view` (
  `USER_PTR` bigint(20),
  `COVER_IMAGE_URL` varchar(232)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `user_talk_not_read_count_view`
--

/*!50001 DROP TABLE IF EXISTS `user_talk_not_read_count_view`*/;
/*!50001 DROP VIEW IF EXISTS `user_talk_not_read_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_talk_not_read_count_view` AS select `talk`.`SEND_USER_PTR` AS `SEND_USER_PTR`,`talk`.`RECEIVE_USER_PTR` AS `RECEIVE_USER_PTR`,count(0) AS `NOT_READ_COUNT` from `user_talk_info` `talk` where (`talk`.`IS_READ_FLG` = '1') group by `talk`.`SEND_USER_PTR`,`talk`.`RECEIVE_USER_PTR` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cover_image_url_view`
--

/*!50001 DROP TABLE IF EXISTS `cover_image_url_view`*/;
/*!50001 DROP VIEW IF EXISTS `cover_image_url_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cover_image_url_view` AS select `m`.`USER_PTR` AS `USER_PTR`,(case when (length(ifnull(`d`.`FILE_NAME`,'')) > 0) then concat('resources',`d`.`URL_PATH`,`m`.`USER_PTR`,'/r_',`d`.`FILE_NAME`) else 'resources/style/images/cover.gif' end) AS `COVER_IMAGE_URL` from (`user_photo_info` `m` left join `user_photo_list_info` `d` on(((`d`.`GROUP_BY_PTR` = `m`.`PKEY`) and (`d`.`IS_COVER` = '1') and (`m`.`NAME` = 'Default')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-13 13:57:36
